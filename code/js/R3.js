const playButton = document.querySelector('.btn')
const videoContainer = document.querySelector('.video-container')
const video = document.querySelector('.video')

const close = document.querySelector('.close')


//volumen de video
video.volume = 0.4

//boton play
playButton.addEventListener('click', () => {
    videoContainer.classList.add('show')
    video.play()
})

//botón cerrar
close.addEventListener('click', () => {
    videoContainer.classList.remove('show')
    video.pause()
})

